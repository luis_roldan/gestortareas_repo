package com.altran.aisw.domain;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.roo.addon.dod.RooDataOnDemand;
import org.springframework.stereotype.Component;

@Configurable
@Component
@RooDataOnDemand(entity = Tarea.class)
public class TareaDataOnDemand {

	private Random rnd = new SecureRandom();

	private List<Tarea> data;

	public Tarea getNewTransientTarea(int index) {
        Tarea obj = new Tarea();
        setArea(obj, index);
        setCliente(obj, index);
        setEstado(obj, index);
        setFechaFin(obj, index);
        setFechaInicio(obj, index);
        setImportancia(obj, index);
        setLogDescription(obj, index);
        setPrioridad(obj, index);
        setProyecto(obj, index);
        setRespCuenta(obj, index);
        setRespOperacion(obj, index);
        setTipoProyecto(obj, index);
        setTitle(obj, index);
        setUrgencia(obj, index);
        return obj;
    }

	public void setArea(Tarea obj, int index) {
        String area = "area_" + index;
        obj.setArea(area);
    }

	public void setCliente(Tarea obj, int index) {
        String cliente = "cliente_" + index;
        obj.setCliente(cliente);
    }

	public void setEstado(Tarea obj, int index) {
        ESTADO estado = ESTADO.class.getEnumConstants()[0];
        obj.setEstado(estado);
    }

	public void setFechaFin(Tarea obj, int index) {
        Date fechaFin = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), Calendar.getInstance().get(Calendar.SECOND) + new Double(Math.random() * 1000).intValue()).getTime();
        obj.setFechaFin(fechaFin);
    }

	public void setFechaInicio(Tarea obj, int index) {
        Date fechaInicio = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), Calendar.getInstance().get(Calendar.SECOND) + new Double(Math.random() * 1000).intValue()).getTime();
        obj.setFechaInicio(fechaInicio);
    }

	public void setImportancia(Tarea obj, int index) {
        IMPORTANCIA importancia = IMPORTANCIA.class.getEnumConstants()[0];
        obj.setImportancia(importancia);
    }

	public void setLogDescription(Tarea obj, int index) {
        String logDescription = "logDescription_" + index;
        obj.setLogDescription(logDescription);
    }

	public void setPrioridad(Tarea obj, int index) {
        PRIORIDAD prioridad = PRIORIDAD.class.getEnumConstants()[0];
        obj.setPrioridad(prioridad);
    }

	public void setProyecto(Tarea obj, int index) {
        String proyecto = "proyecto_" + index;
        obj.setProyecto(proyecto);
    }

	public void setRespCuenta(Tarea obj, int index) {
        String respCuenta = "respCuenta_" + index;
        obj.setRespCuenta(respCuenta);
    }

	public void setRespOperacion(Tarea obj, int index) {
        String respOperacion = "respOperacion_" + index;
        obj.setRespOperacion(respOperacion);
    }

	public void setTipoProyecto(Tarea obj, int index) {
        TIPO_PROYECTO tipoProyecto = TIPO_PROYECTO.class.getEnumConstants()[0];
        obj.setTipoProyecto(tipoProyecto);
    }

	public void setTitle(Tarea obj, int index) {
        String title = "title_" + index;
        obj.setTitle(title);
    }

	public void setUrgencia(Tarea obj, int index) {
        URGENTE urgencia = URGENTE.class.getEnumConstants()[0];
        obj.setUrgencia(urgencia);
    }

	public Tarea getSpecificTarea(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        Tarea obj = data.get(index);
        Long id = obj.getId();
        return Tarea.findTarea(id);
    }

	public Tarea getRandomTarea() {
        init();
        Tarea obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return Tarea.findTarea(id);
    }

	public boolean modifyTarea(Tarea obj) {
        return false;
    }

	public void init() {
        int from = 0;
        int to = 10;
        data = Tarea.findTareaEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'Tarea' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<Tarea>();
        for (int i = 0; i < 10; i++) {
            Tarea obj = getNewTransientTarea(i);
            try {
                obj.persist();
            } catch (ConstraintViolationException e) {
                StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getConstraintDescriptor()).append(":").append(cv.getMessage()).append("=").append(cv.getInvalidValue()).append("]");
                }
                throw new RuntimeException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
}
