package com.altran.aisw.domain;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.roo.addon.test.RooIntegrationTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
@RooIntegrationTest(entity = Tarea.class)
public class TareaIntegrationTest {

    @Test
    public void testMarkerMethod() {
    }

	@Autowired
    private TareaDataOnDemand dod;

	@Test
    public void testCountTareas() {
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", dod.getRandomTarea());
        long count = Tarea.countTareas();
        Assert.assertTrue("Counter for 'Tarea' incorrectly reported there were no entries", count > 0);
    }

	@Test
    public void testFindTarea() {
        Tarea obj = dod.getRandomTarea();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to provide an identifier", id);
        obj = Tarea.findTarea(id);
        Assert.assertNotNull("Find method for 'Tarea' illegally returned null for id '" + id + "'", obj);
        Assert.assertEquals("Find method for 'Tarea' returned the incorrect identifier", id, obj.getId());
    }

	@Test
    public void testFindAllTareas() {
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", dod.getRandomTarea());
        long count = Tarea.countTareas();
        Assert.assertTrue("Too expensive to perform a find all test for 'Tarea', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<Tarea> result = Tarea.findAllTareas();
        Assert.assertNotNull("Find all method for 'Tarea' illegally returned null", result);
        Assert.assertTrue("Find all method for 'Tarea' failed to return any data", result.size() > 0);
    }

	@Test
    public void testFindTareaEntries() {
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", dod.getRandomTarea());
        long count = Tarea.countTareas();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<Tarea> result = Tarea.findTareaEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'Tarea' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'Tarea' returned an incorrect number of entries", count, result.size());
    }

	@Test
    public void testFlush() {
        Tarea obj = dod.getRandomTarea();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to provide an identifier", id);
        obj = Tarea.findTarea(id);
        Assert.assertNotNull("Find method for 'Tarea' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifyTarea(obj);
        Integer currentVersion = obj.getVersion();
        obj.flush();
        Assert.assertTrue("Version for 'Tarea' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

	@Test
    public void testMergeUpdate() {
        Tarea obj = dod.getRandomTarea();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to provide an identifier", id);
        obj = Tarea.findTarea(id);
        boolean modified =  dod.modifyTarea(obj);
        Integer currentVersion = obj.getVersion();
        Tarea merged = obj.merge();
        obj.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'Tarea' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

	@Test
    public void testPersist() {
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", dod.getRandomTarea());
        Tarea obj = dod.getNewTransientTarea(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'Tarea' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'Tarea' identifier to be null", obj.getId());
        obj.persist();
        obj.flush();
        Assert.assertNotNull("Expected 'Tarea' identifier to no longer be null", obj.getId());
    }

	@Test
    public void testRemove() {
        Tarea obj = dod.getRandomTarea();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Tarea' failed to provide an identifier", id);
        obj = Tarea.findTarea(id);
        obj.remove();
        obj.flush();
        Assert.assertNull("Failed to remove 'Tarea' with identifier '" + id + "'", Tarea.findTarea(id));
    }
}
