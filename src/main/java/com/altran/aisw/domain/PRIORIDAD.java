package com.altran.aisw.domain;


public enum PRIORIDAD {

    V_High, High, Medium, Low;
}
