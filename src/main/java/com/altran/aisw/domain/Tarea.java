package com.altran.aisw.domain;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@Entity
@Configurable
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Tarea {

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date fechaInicio;

    @Enumerated
    private ESTADO estado;
    private String title;
    private String proyecto;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date fechaFin;

    @Enumerated
    private PRIORIDAD prioridad;

    @Enumerated
    private IMPORTANCIA importancia;

    @Enumerated
    private URGENTE urgencia;

    private String cliente;

    

    @Enumerated
    private TIPO_PROYECTO tipoProyecto;

   

    private String logDescription;

    private String respOperacion;

    private String area;

    private String respCuenta;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

	@Version
    @Column(name = "version")
    private Integer version;

	public Long getId() {
        return this.id;
    }

	public void setId(Long id) {
        this.id = id;
    }

	public Integer getVersion() {
        return this.version;
    }

	public void setVersion(Integer version) {
        this.version = version;
    }

	public Date getFechaInicio() {
        return this.fechaInicio;
    }

	public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

	public ESTADO getEstado() {
        return this.estado;
    }

	public void setEstado(ESTADO estado) {
        this.estado = estado;
    }

	public Date getFechaFin() {
        return this.fechaFin;
    }

	public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

	public PRIORIDAD getPrioridad() {
        return this.prioridad;
    }

	public void setPrioridad(PRIORIDAD prioridad) {
        this.prioridad = prioridad;
    }

	public IMPORTANCIA getImportancia() {
        return this.importancia;
    }

	public void setImportancia(IMPORTANCIA importancia) {
        this.importancia = importancia;
    }

	public URGENTE getUrgencia() {
        return this.urgencia;
    }

	public void setUrgencia(URGENTE urgencia) {
        this.urgencia = urgencia;
    }

	public String getCliente() {
        return this.cliente;
    }

	public void setCliente(String cliente) {
        this.cliente = cliente;
    }

	public String getProyecto() {
        return this.proyecto;
    }

	public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

	public TIPO_PROYECTO getTipoProyecto() {
        return this.tipoProyecto;
    }

	public void setTipoProyecto(TIPO_PROYECTO tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

	public String getTitle() {
        return this.title;
    }

	public void setTitle(String title) {
        this.title = title;
    }

	public String getLogDescription() {
        return this.logDescription;
    }

	public void setLogDescription(String logDescription) {
        this.logDescription = logDescription;
    }

	public String getRespOperacion() {
        return this.respOperacion;
    }

	public void setRespOperacion(String respOperacion) {
        this.respOperacion = respOperacion;
    }

	public String getArea() {
        return this.area;
    }

	public void setArea(String area) {
        this.area = area;
    }

	public String getRespCuenta() {
        return this.respCuenta;
    }

	public void setRespCuenta(String respCuenta) {
        this.respCuenta = respCuenta;
    }

	@PersistenceContext
    transient EntityManager entityManager;

	public static final EntityManager entityManager() {
        EntityManager em = new Tarea().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }

	public static long countTareas() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tarea o", Long.class).getSingleResult();
    }

	public static List<Tarea> findAllTareas() {
        return entityManager().createQuery("SELECT o FROM Tarea o", Tarea.class).getResultList();
    }

	public static Tarea findTarea(Long id) {
        if (id == null) return null;
        return entityManager().find(Tarea.class, id);
    }

	public static List<Tarea> findTareaEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tarea o", Tarea.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

	@Transactional
    public void persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }

	@Transactional
    public void remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tarea attached = Tarea.findTarea(this.id);
            this.entityManager.remove(attached);
        }
    }

	@Transactional
    public void flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }

	@Transactional
    public void clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }

	@Transactional
    public Tarea merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tarea merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }

	public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
