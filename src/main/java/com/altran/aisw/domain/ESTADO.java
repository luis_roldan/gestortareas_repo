package com.altran.aisw.domain;


public enum ESTADO {

    Nueva, EnProgreso, Cerrada, Parada;
}
