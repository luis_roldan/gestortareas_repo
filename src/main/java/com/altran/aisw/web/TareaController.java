package com.altran.aisw.web;

import com.altran.aisw.domain.ESTADO;
import com.altran.aisw.domain.IMPORTANCIA;
import com.altran.aisw.domain.PRIORIDAD;
import com.altran.aisw.domain.TIPO_PROYECTO;
import com.altran.aisw.domain.Tarea;
import com.altran.aisw.domain.URGENTE;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

@RequestMapping("/tareas")
@Controller
@RooWebScaffold(path = "tareas", formBackingObject = Tarea.class)
public class TareaController {

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Tarea tarea, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, tarea);
            return "tareas/create";
        }
        uiModel.asMap().clear();
        tarea.persist();
        return "redirect:/tareas/" + encodeUrlPathSegment(tarea.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Tarea());
        return "tareas/create";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("tarea", Tarea.findTarea(id));
        uiModel.addAttribute("itemId", id);
        return "tareas/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("tareas", Tarea.findTareaEntries(firstResult, sizeNo));
            float nrOfPages = (float) Tarea.countTareas() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("tareas", Tarea.findAllTareas());
        }
        addDateTimeFormatPatterns(uiModel);
        return "tareas/list";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Tarea tarea, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, tarea);
            return "tareas/update";
        }
        uiModel.asMap().clear();
        tarea.merge();
        return "redirect:/tareas/" + encodeUrlPathSegment(tarea.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Tarea.findTarea(id));
        return "tareas/update";
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Tarea tarea = Tarea.findTarea(id);
        tarea.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/tareas";
    }

	void addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("tarea_fechainicio_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute("tarea_fechafin_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }

	void populateEditForm(Model uiModel, Tarea tarea) {
        uiModel.addAttribute("tarea", tarea);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("estadoitems", Arrays.asList(ESTADO.values()));
        uiModel.addAttribute("importancias", Arrays.asList(IMPORTANCIA.values()));
        uiModel.addAttribute("prioridads", Arrays.asList(PRIORIDAD.values()));
        uiModel.addAttribute("tipo_proyectoitems", Arrays.asList(TIPO_PROYECTO.values()));
        uiModel.addAttribute("urgentes", Arrays.asList(URGENTE.values()));
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
