package com.altran.aisw.web;

import com.altran.aisw.domain.Tarea;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.roo.addon.web.mvc.controller.converter.RooConversionService;

@Configurable
/**
 * A central place to register application converters and formatters. 
 */
@RooConversionService
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
	}

	public Converter<Tarea, String> getTareaToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.altran.aisw.domain.Tarea, java.lang.String>() {
            public String convert(Tarea tarea) {
                return new StringBuilder().append(tarea.getFechaInicio()).append(" ").append(tarea.getFechaFin()).append(" ").append(tarea.getCliente()).append(" ").append(tarea.getProyecto()).toString();
            }
        };
    }

	public Converter<Long, Tarea> getIdToTareaConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.altran.aisw.domain.Tarea>() {
            public com.altran.aisw.domain.Tarea convert(java.lang.Long id) {
                return Tarea.findTarea(id);
            }
        };
    }

	public Converter<String, Tarea> getStringToTareaConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.altran.aisw.domain.Tarea>() {
            public com.altran.aisw.domain.Tarea convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Tarea.class);
            }
        };
    }

	public void installLabelConverters(FormatterRegistry registry) {
        registry.addConverter(getTareaToStringConverter());
        registry.addConverter(getIdToTareaConverter());
        registry.addConverter(getStringToTareaConverter());
    }

	public void afterPropertiesSet() {
        super.afterPropertiesSet();
        installLabelConverters(getObject());
    }
}
