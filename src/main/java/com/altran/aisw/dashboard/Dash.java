package com.altran.aisw.dashboard;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.altran.aisw.domain.ESTADO;
import com.altran.aisw.domain.PRIORIDAD;
import com.altran.aisw.domain.Tarea;

/**
 * Servlet implementation class Dash
 */
public class Dash extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Dash() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		List<Tarea> ts = Tarea.findAllTareas();
		// calculo tareas
		int todas = ts.size();
		StringBuilder data = new StringBuilder();
		// VHigh','High', 'Medium','Low'
		HashMap<String, Integer> tot = new HashMap<String, Integer>();
		tot.put("VHigh", 0);
		tot.put("High", 0);
		tot.put("Medium", 0);
		tot.put("Low", 0);
		HashMap<String, Integer> ab = new HashMap<String, Integer>();
		ab.put("VHigh", 0);
		ab.put("High", 0);
		ab.put("Medium", 0);
		ab.put("Low", 0);

		for (Tarea t : ts) {
			if (t.getEstado() != ESTADO.Cerrada) {
				if (t.getPrioridad() == PRIORIDAD.V_High) {
					ab.put("VHigh", ab.get("VHigh") + 1);
					tot.put("VHigh", tot.get("VHigh") + 1);
				} else if (t.getPrioridad() == PRIORIDAD.High) {
					ab.put("High", ab.get("High") + 1);
					tot.put("High", tot.get("High") + 1);
				} else if (t.getPrioridad() == PRIORIDAD.Medium) {
					ab.put("Medium", ab.get("Medium") + 1);
					tot.put("Medium", tot.get("Medium") + 1);
				} else if (t.getPrioridad() == PRIORIDAD.Low) {
					ab.put("Low", ab.get("Low") + 1);
					tot.put("Low", tot.get("Low") + 1);
				}
			} else {
				if (t.getPrioridad() == PRIORIDAD.V_High) {

					tot.put("VHigh", tot.get("VHigh") + 1);
				} else if (t.getPrioridad() == PRIORIDAD.High) {

					tot.put("High", tot.get("High") + 1);
				} else if (t.getPrioridad() == PRIORIDAD.Medium) {

					tot.put("Medium", tot.get("Medium") + 1);
				} else if (t.getPrioridad() == PRIORIDAD.Low) {

					tot.put("Low", tot.get("Low") + 1);
				}
			}

		}
		
		data.append(todas);
		data.append(";");
		data.append(tot.get("VHigh"));
		data.append(",");
		data.append(tot.get("High"));
		data.append(",");
		data.append(tot.get("Medium"));
		data.append(",");
		data.append(tot.get("Low"));
		data.append(";");
		data.append(ab.get("VHigh"));
		data.append(",");
		data.append(ab.get("High"));
		data.append(",");
		data.append(ab.get("Medium"));
		data.append(",");
		data.append(ab.get("Low"));
		
		response.getWriter().println(data.toString());

		System.out.println("retornando");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
